import Head from "next/head";
import Login from "../components/Login";
import { useMoralis } from "react-moralis";
import Header from "../components/Header";
import Messages from "../components/Messages";
import Image from "next/image";

export default function Home() {
  const { isAuthenticated, logout, user } = useMoralis();

  if (!isAuthenticated) return <Login />;

  return (
    <div className="h-screen overflow-y-scroll z-50 bg-gradient-to-b from-fuchsia-900 to-indigo-900 overflow-hidden relative">
      <Head>
        <title>Metaverse Challenge</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="max-w-screen-2xl mx-auto">
        <div className="p-4">
          <Header />
        </div>
        <Messages />
      </div>
      <Image
        src="https://images.theconversation.com/files/414962/original/file-20210806-17-jibbct.jpg"
        layout="fill"
        objectFit="cover"
        className="opacity-5"
      />
    </div>
  );
}
