import { useState } from "react";
import { useMoralis } from "react-moralis";

function ChangeUsername() {
  const { setUserData, isUserUpdating, userError, user } = useMoralis();
  const [showModal, setShowModal] = useState(false);
  const [newUserName, setNewUsername] = useState("");

  const setUsername = (e) => {
    e.preventDefault();

    if (!newUserName) return;
    setUserData({ username: newUserName });
    setShowModal(false);
    setNewUsername("");
  };

  return (
    <>
      <div className="text-sm absolute top-5 right-5">
        <button
          disabled={isUserUpdating}
          onClick={() => setShowModal(true)}
          className="hover:text-pink-700 bg-gray-900 rounded-full py-3 px-5"
          id="open-modal"
        >
          Change your username
        </button>
      </div>
      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto lg:max-w-3xl max-w-[350px]">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-black outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-gray-900 rounded-t">
                  <h3 className="text-3xl font-semibold text-center">
                    Enter your new Username
                  </h3>
                  <button
                    className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                    onClick={() => setShowModal(false)}
                  >
                    <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                      ×
                    </span>
                  </button>
                </div>
                <div className="relative p-6 flex-auto">
                  <input
                    className="outline-none w-full border-b-2 border-pink-500 py-4 bg-transparent"
                    type="text"
                    value={newUserName}
                    placeholder={`current username : ${user.getUsername()}`}
                    onChange={(e) => setNewUsername(e.target.value)}
                  />
                </div>
                {/*footer*/}
                <div className="flex items-center justify-end p-6 border-t border-solid border-gray-900 rounded-b">
                  <button
                    className="text-indigo-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                    type="button"
                    onClick={() => setShowModal(false)}
                  >
                    Close
                  </button>
                  <button
                    className="bg-pink-500 text-white hover:bg-pink-600 active:bg-pink-700 font-bold uppercase text-sm px-6 py-3 shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 rounded-full disabled:bg-slate-800 disabled:text-slate-900"
                    type="submit"
                    onClick={setUsername}
                    disabled={!newUserName}
                  >
                    Save Changes
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-50 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
}

export default ChangeUsername;
