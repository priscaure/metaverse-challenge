import Image from "next/image";
import { useMoralis } from "react-moralis";

function Login() {
  const { authenticate, isAuthenticating } = useMoralis();

  return (
    <div className="bg-black relative">
      <div className="flex flex-col absolute z-50 h-5/6 w-full items-center justify-center space-y-8">
        <h1 className="text-6xl text-white font-bold uppercase text-center mt-8">
          Welcome <br />
          to the metaverse
        </h1>
        <button
          onClick={authenticate}
          className="bg-yellow-500 rounded-lg p-5 font-bold animate-pulse inline-flex items-center"
        >
          {!isAuthenticating ? (
            "Discover"
          ) : (
            <>
              <svg
                class="animate-spin -ml-1 mr-3 h-5 w-5"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
              >
                <circle
                  class="opacity-25"
                  cx="12"
                  cy="12"
                  r="10"
                  stroke="currentColor"
                  stroke-width="4"
                ></circle>
                <path
                  class="opacity-75"
                  fill="currentColor"
                  d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                ></path>
              </svg>
              Processing...
            </>
          )}
        </button>
      </div>
      <div className="w-full h-screen relative">
        <div className="bg-gradient-to-b from-black rounded-xl opacity-30 w-full h-full absolute z-30" />
        <Image
          src="https://images.theconversation.com/files/414962/original/file-20210806-17-jibbct.jpg"
          layout="fill"
          objectFit="cover"
        />
      </div>
    </div>
  );
}

export default Login;
